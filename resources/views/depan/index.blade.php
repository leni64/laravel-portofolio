<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Portofolio</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <style>
    
    .container .image-container{
      columns: 3 250px;
      gap: 15px;
    }

    .container .image-container img{
      margin-bottom: 10px;
      border-radius: 5px;
      width: 100%;
    }

  </style>
  <!-- Favicons -->
  <link href="{{ asset('depan') }}/assets/img/favicon.png" rel="icon">
  <link href="{{ asset('depan') }}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Satisfy" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('depan') }}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ asset('depan') }}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{ asset('depan') }}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{ asset('depan') }}/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="{{ asset('depan') }}/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/devicons/devicon@v2.15.1/devicon.min.css">
  <!-- Template Main CSS File -->
  <link href="{{ asset('depan') }}/assets/css/style.css" rel="stylesheet">
  
  <!-- =======================================================
  * Template Name: Laura - v4.9.1
  * Template URL: https://bootstrapmade.com/laura-free-creative-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex justify-content-center align-items-center header-transparent">

    <nav id="navbar" class="navbar" style="background-color: black">
      <ul>
        <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
        <li><a class="nav-link scrollto" href="/#about">About</a></li>
        <li><a class="nav-link scrollto" href="/#education">Resume</a></li>
        <li><a class="nav-link scrollto" href="/#awards">Awards</a></li>
        <li><a class="nav-link scrollto " href="/#skills">Skill</a></li>
      </ul>
      <i class="bi bi-list mobile-nav-toggle"></i>
    </nav><!-- .navbar -->

  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container">
      <h1>Leni</h1>
      <h2>I'm a Student In Polytechnic of Sambas</h2>
      <a href="#about" class="btn-scroll scrollto" title="Scroll Down"><i class="bx bx-chevron-down"></i></a>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= About Me Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="section-title">
          <span>About Me</span>
          <h2>About Me</h2>
          <p>{{get_meta_value('_kota') }} - {{get_meta_value('_provinsi')}} - {{get_meta_value('_nohp') }} - 
            {{get_meta_value('_email') }}</p>
        </div>

        <div class="row">
          <div class="container" style="display: flex">
            <img class="img-fluid img-profile rounded-circle mx-auto mb-1" src="{{ asset('foto')."/".get_meta_value('_foto') }}" alt="..." />
          
            <div class="col-lg-8 d-flex flex-column align-items-stretch">
              <div class="content ps-lg-4 d-flex flex-column justify-content-center">
                <div class="row">
                  <div class="col-lg-6">
                    <ul>
                      <li><i class="bi bi-chevron-right"></i> <strong>Name:</strong> <span>Leni</span></li>
                      <li><i class="bi bi-chevron-right"></i> <strong>Website:</strong> <span>https://leni800.wordpress.com/</span></li>
                      <li><i class="bi bi-chevron-right"></i> <strong>Phone:</strong> <span>+62 8984574129</span></li>
                      <li><i class="bi bi-chevron-right"></i> <strong>City:</strong> <span>West Kalimantan, ID</span></li>
                    </ul>
                  </div>
                  <div class="col-lg-6">
                    <ul>
                      <li><i class="bi bi-chevron-right"></i> <strong>Age:</strong> <span>19</span></li>
                      <li><i class="bi bi-chevron-right"></i> <strong>Birth of date:</strong> <span>April 6, 2003</span></li>
                      <li><i class="bi bi-chevron-right"></i> <strong>Email:</strong> <span>leniwfm@gmail.com</span></li>
                      <li><i class="bi bi-chevron-right"></i> <strong>Freelance:</strong> <span>Available</span></li>
                    </ul>
                  </div>
                </div>
                <div class="row mt-n4">
                  <div class="col-md-6 mt-5 d-md-flex align-items-md-stretch">
                    <div class="count-box">
                      <i class="bi bi-emoji-smile" style="color: #20b38e;"></i>
                      <span data-purecounter-start="0" data-purecounter-end="232" data-purecounter-duration="1" class="purecounter"></span>
                      <p><strong>Happy Clients</strong> Mampu berkomunikasi dengan baik dan mudah berbaur</p>
                    </div>
                  </div>
  
                  <div class="col-md-6 mt-5 d-md-flex align-items-md-stretch">
                    <div class="count-box">
                      <i class="bi bi-journal-richtextr" style="color: #8a1ac2;"></i>
                      <span data-purecounter-start="0" data-purecounter-end="521" data-purecounter-duration="1" class="purecounter"></span>
                      <p><strong>Projects</strong> Mampu Berkerja Sama dalam Tim</p>
                    </div>
                  </div>
  
                  <div class="col-md-6 mt-5 d-md-flex align-items-md-stretch">
                    <div class="count-box">
                      <i class="bi bi-clock" style="color: #2cbdee;"></i>
                      <span data-purecounter-start="0" data-purecounter-end="18" data-purecounter-duration="1" class="purecounter"></span>
                      <p><strong>Years of experience</strong>Sudah Berpengalaman selama 1,5 tahun</p>
                    </div>
                  </div>
  
                </div>
              </div>
          </div>
          <!-- End .content-->
          
        </div>
      </div>
      </div>
    </section><!-- End About Me Section -->

    <!-- ======= My Resume Section ======= -->
    <section id="education" class="education">
      <div class="container">

        <div class="section-title">
          <span>My Resume & Education</span>
          <h2>My Resume & Education</h2>
        </div>

        <div class="row">
          <div class="col-lg-6">
            <h3 class="resume-title">Sumary</h3>
            <div class="resume-item pb-0">
              <h4>LENI</h4>
              <p><em>{!!$about->isi!!}</em></p>
              <p>
              <ul>
                <li>Sambas, West Kalimantan</li>
                <li>+628984574129</li>
                <li>leniwfm@gmail.com</li>
              </ul>
              </p>
            </div>

            <h3 class="resume-title">Education</h3>
            <div class="resume-item">
              <h4>SDN Sembuak Segantong </h4>
              <h5>2010 - 2015</h5>
              <p><em>Sembuak Segantong, Tanjung Keracut, Teluk Keramat</em></p>
              <p>Pada kelas 1 saya sekolah di Sembuak, Namun pada Kelas 2 SD saya pindah ke SDN Selakau Timur.
                Tetapi Kelas 3-nya saya pindah lagi ke SD Sembuak
              </p>
            </div>

            <div class="resume-item">
              <h4>SMPN 1 Teluk Keramat</h4>
              <h5>2015 - 2018</h5>
              <p><em>Sekura, Teluk Keramat</em></p>
              <p>Saya Pernah Ikut Lomba Pidato dan Nilai Ujian Bahasa Inggris saya pernah menduduki Peringkat 3 satu sekolah</p>
            </div>
            <div class="col-lg-6">
              <h3 class="resume-title">SMAN 1 Sambas</h3>
              <div class="resume-item">
                <h4>Matematika dan Ilmu Pengetahuan Alam</h4>
                <h5>2019 - 2021</h5>
                <p><em>Sambas, Kalimatan Barat </em></p>
                <p>Mata Pelajaran Favorit</p>
                <p>
                <ul>
                  <li>Biologi</li>
                  <li>Bahasa Inggris</li>
                  <li>Bahasa Indonesia</li>
                  <li>Matematika</li>
                </ul>
                </p>
              </div>
          </div>
         
            <div class="resume-item">
              <h4>Politeknik Negeri Sambas</h4>
              <h5>2021 - Sekarang</h5>
              <p><em>Senyawan, Sejangkung, Sambas</em></p>
              <p>
              <ul>
                <li>Mampu mengunakan microsoft office word, excel dan power point dengan baik.</li>
                <li>Mampu menyelesaikan project membuat aplikasi Rekam Medis dan Perpustakaan.</li>
                <li>Mampu mengkonfigurasi DNS Server, Proxy Server, FTP Server, Web Server, Mail Server</li>
                <li>Mampu membuat Proposal dan Presentasi dalam sebulan</li>
              </ul>
              </p>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End My Resume Section -->

    <!-- ======= My Services Section ======= -->
    <section id="awards" class="awards">
      <div class="container">

        <div class="section-title">
          <span>My Awards</span>
          <h2>My Awards</h2>
          <p>Berikut beberapa penghargaan saya selama beberapa tahun:</p>
        </div>
        <!-- Awards-->
          <div class="resume-section-content">
              <h2 class="mb-5">{{ $awards->judul}}</h2>
              {!! set_list_awards($awards->isi)!!}
          </div>
          
      </div>
    </section><!-- End My Services Section -->

    <!-- ======= My Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container">

        <div class="section-title">
          <span>My Skills</span>
          <h2>My Skillls</h2>
          <p>Berikut beberapa skills yang saya kembangkan:</p>
        </div>

       <!-- Skills-->
       <section class="resume-section" id="skills">
        <div class="resume-section-content">
            <h2 class="mb-5">Keterampilan</h2>
            <div class="subheading mb-3">Programming Languages & Tools</div>
            
            <ul class="list-inline dev-icons">
            @foreach (explode (',', get_meta_value('_language')) as $item)
            <li class="list-inline-item"><i class="devicon-{{ strtolower($item) }}-plain"></i></li>
            @endforeach
                
            </ul>
            <div class="subheading mb-3">Workflow</div>
            {!! set_list_workflow(get_meta_value('_workflow')) !!}
            
        </div>
        </div>
      </div>
    </section><!-- End My Portfolio Section -->

<!-- ======= My Portfolio Section ======= -->
<section id="portfolio" class="portfolio">
  <div class="container">

    <div class="section-title">
      <span>My Gallery</span>
      <h2>My Gallery</h2>
      <p>Beberapa foto semasa kuliah:</p>
    </div>

    <ul id="portfolio-flters" class="d-flex justify-content-center">
      <li data-filter="*" class="filter-active">All</li>
    </ul>

    <div class="row portfolio-container">

      <div class="col-lg-4 col-md-6 portfolio-item filter-app">
        <div class="portfolio-img"><img src="{{ asset('depan') }}/assets/img/pic1.jpg" class="img-fluid" alt=""></div>
        <div class="portfolio-info">
          <h4>App 1</h4>
          <p>App</p>
          <a href="{{ asset('depan') }}/assets/img/pic1.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 1"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-web">
        <div class="portfolio-img"><img src="{{ asset('depan') }}/assets/img/pic4.jpg" class="img-fluid" alt=""></div>
        <div class="portfolio-info">
          <h4>Web 3</h4>
          <p>Web</p>
          <a href="{{ asset('depan') }}/assets/img/pic4.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-app">
        <div class="portfolio-img"><img src="{{ asset('depan') }}/assets/img/pic6.jpg" class="img-fluid" alt=""></div>
        <div class="portfolio-info">
          <h4>App 2</h4>
          <p>App</p>
          <a href="{{ asset('depan') }}/assets/img/pic6.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 2"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-card">
        <div class="portfolio-img"><img src="{{ asset('depan') }}/assets/img/pic2.jpg" class="img-fluid" alt=""></div>
        <div class="portfolio-info">
          <h4>Card 2</h4>
          <p>Card</p>
          <a href="{{ asset('depan') }}/assets/img/pic2.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 2"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-web">
        <div class="portfolio-img"><img src="{{ asset('depan') }}/assets/img/pic3.jpg" class="img-fluid" alt=""></div>
        <div class="portfolio-info">
          <h4>Web 2</h4>
          <p>Web</p>
          <a href="{{ asset('depan') }}/assets/img/pic3.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 2"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-app">
        <div class="portfolio-img"><img src="{{ asset('depan') }}/assets/img/pic8.jpg" class="img-fluid" alt=""></div>
        <div class="portfolio-info">
          <h4>App 3</h4>
          <p>App</p>
          <a href="{{ asset('depan') }}/assets/img/pic8.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 3"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>

    </div>

  </div>
</section><!-- End My Portfolio Section -->
    <!-- ======= Contact Me Section ======= -->
    <section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <span>Contact Me</span>
          <h2>Contact Me</h2>
        </div>

        <div class="row">

          <div class="col-lg-6">

            <div class="row">
              <div class="col-md-12">
                <div class="info-box">
                  <i class="bx bx-share-alt"></i>
                  <h3>Social Profiles</h3>
                  <div class="social-links">
                    <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
                    <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
                    <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
                    <a href="#" class="google-plus"><i class="bi bi-skype"></i></a>
                    <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="info-box mt-4">
                  <i class="bx bx-envelope"></i>
                  <h3>Email Me</h3>
                  <p>leniwfm@gmail.com</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="info-box mt-4">
                  <i class="bx bx-phone-call"></i>
                  <h3>Call Me</h3>
                  <p>+628984574129</p>
                </div>
              </div>
            </div>

          </div>

          <div class="col-lg-6">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="row">
                <div class="col-md-6 form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required>
                </div>
                <div class="col-md-6 form-group mt-3 mt-md-0">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required>
                </div>
              </div>
              <div class="form-group mt-3">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
              </div>
              <div class="form-group mt-3">
                <textarea class="form-control" name="message" rows="6" placeholder="Message" required></textarea>
              </div>
              <div class="my-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div>
      

        </div>

      </div>
    </section><!-- End Contact Me Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <h3>LENI</h3>
      <p>For more information</p>
      <div class="social-links">
        <a class="social-links" href="{{ get_meta_value('_github')}}"><i class="fab fa-github"></i></a>
        <a class="social-links" href="{{ get_meta_value('_facebook')}}"><i class="fab fa-facebook"></i></a>
        <a class="social-links" href="{{ get_meta_value('_instagram')}}"><i class="fab fa-instagram"></i></a>
      </div>
      <div class="copyright">
        &copy; Copyright <strong><span>Leni</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/laura-free-creative-bootstrap-theme/ -->
        Designed by <a>LeniMIF3B</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ asset('depan') }}/assets/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="{{ asset('depan') }}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{ asset('depan') }}/assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="{{ asset('depan') }}/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="{{ asset('depan') }}/assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="{{ asset('depan') }}/assets/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="{{ asset('depan') }}/assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('depan') }}/{{ asset('depan') }}/assets/js/main.js"></script>

</body>

</html>